# Requisitos de usuario

* CRUD de libros 
* CRUD de autores
* Indicar la posición del libro en la biblioteca personal
* Indicar si un libro está leido (Si/No)
* Indicar si el libro está sacado de una biblioteca u otro lugar (enum)

## Datos

```plantuml

@startuml
skinparam backgroundColor #EEE
skinparam handwritten true

class Book{
    - id
    - name
    - position
    - from
    - readed
}

class Author {
    - id
    - name
}

Book o--> Author

@enduml

```
