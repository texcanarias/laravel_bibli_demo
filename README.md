#biblio
Sistema básico para desarrollar pruebas.


#Notas
## Instalaciones básicas

### Instalación básica de php 7.3
https://computingforgeeks.com/how-to-install-php-7-3-on-debian-9-debian-8/

### Instalación del proyecto inicial laravel
composer create-project --prefer-dist laravel/laravel .
https://hub.docker.com/r/devdockerrs/php7.3-apache2.4/dockerfile

## Apuntes docker

### Contruir las imágenes
docker-compose build

### Lista los contenedores
docker ps

###Ejecutar el bash en una imagen
docker exec -t -i container_name /bin/bash

###Eliminar ficheros temporales
docker-compose rm -v

###Arrancar las imagenes
docker-composer up

###Parar las imagenes
docker-composer down

#Instalación básica
* composer install
* chmod 777 storage -R
* chmod 777 bootstrap/cache -R

##Authentication
* php artisan make:auth (solo una vez)
* php artisan migrate:fresh --seed 
* php artisan migrate:reset (en el caso de querer dar marcha atrás)
* php artisan db:seed  //Rellenar de datos de ejemplo las tablas

##Creación de controller
* php artisan make:controller ContactController --resource (crea un controller con los metodos por defecto)

## Ejecución de pruebas unitarias
Para una correcta ejecución de las pruebas unitarias se debe reconstuir la base de datos 
mediante el comando
* php artisan migrate:fresh --seed

## Nota sobre llamadas REST desde Postman
Las llamadas a PUT desde Postman no funcionan.
Para solucionar este problema hay que:
* Hacer uso de POST
* Pasar como parámeto _method = PUT
* Poner en el header Content-Type = application/x-www-form-urlencoded