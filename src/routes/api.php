<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('1.0/books', 'BooksController@index');
Route::get('1.0/books/readed', 'BooksController@readed');
Route::get('1.0/books/{id}', 'BooksController@show');
Route::post('1.0/books', 'BooksController@store');
Route::put('1.0/books/{id}', 'BooksController@update');
Route::patch('1.0/books/{id}', 'BooksController@update');
Route::delete('1.0/books/{id}', 'BooksController@destroy');

Route::get('1.0/authors', 'AuthorsController@index');
Route::get('1.0/authors/{id}', 'AuthorsController@show');
Route::post('1.0/authors', 'AuthorsController@store');
Route::put('1.0/authors/{id}', 'AuthorsController@update');
Route::patch('1.0/authors/{id}', 'AuthorsController@update');
Route::delete('1.0/authors/{id}', 'AuthorsController@destroy');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});