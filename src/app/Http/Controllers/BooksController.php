<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Book;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            if(true){

                $books = Book::all();                

                return response()->json($books, 200);
            } else {
                return response()->json(["message" => "Unauthenticated."], 401);
            }
        } catch (QueryException $ex) {
            return response()->json(['error' => 'Data base error.'], 500);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Error en la consulta'], 400);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function readed()
    {
        try {
            if(true){
                $books = Book::where('readed',true)->get();
                if(0 == $books->count()){
                    return response()->json(["message" => "Not found."], 404);    
                }
                return response()->json($books[0], 200);
            } else {
                return response()->json(["message" => "Unauthenticated."], 401);
            }
        } catch (QueryException $ex) {
            return response()->json(['error' => 'Data base error.'], 500);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Error en la consulta'], 400);
        }
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if(true){
                $book = new Book();
                $book->name = $request->input('name');
                if($request->has('position')){
                    $book->position = $request->input('position');
                }
                if($request->has('from')){
                    $book->from = $request->input('from');
                }
                if($request->has('readed')){
                    $book->readed = $request->input('readed');
                }
                if($request->has('author_id')){
                    $book->author_id = $request->input('author_id');
                }

                $book->save();

                return response()->json($book, 200);
            } else {
                return response()->json(["message" => "Unauthenticated."], 401);
            }
        } catch (QueryException $ex) {
            return response()->json(['error' => 'Data base error.'], 500);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Error en la consulta'], 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if(true){
                $book = Book::find($id);
                return response()->json($book, 200);
            } else {
                return response()->json(["message" => "Unauthenticated."], 401);
            }
        } catch (QueryException $ex) {
            return response()->json(['error' => 'Data base error.'], 500);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Error en la consulta'], 400);
        }        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            if(true){
                $book = Book::find($id);
                
                if($request->has('name')){
                    $book->name = $request->input('name');
                }
                if($request->has('position')){
                    $book->position = $request->input('position');
                }
                if($request->has('from')){
                    $book->from = $request->input('from');
                }
                if($request->has('readed')){
                    $book->readed = $request->input('readed');
                }
                if($request->has('author_id')){
                    $book->author_id = $request->input('author_id');
                }

                $book->save();                
                return response()->json($book, 200);
            } else {
                return response()->json(["message" => "Unauthenticated."], 401);
            }
        } catch (QueryException $ex) {
            print_r($ex);
            return response()->json(['error' => 'Data base error.'], 500);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Error en la consulta'], 400);
        }  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if(true){
                Book::where('id', $id)->delete();
                return response()->json([], 200);
            } else {
                return response()->json(["message" => "Unauthenticated."], 401);
            }
        } catch (QueryException $ex) {
            return response()->json(['error' => 'Data base error.'], 500);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Error en la consulta'], 400);
        }  
    }
}
