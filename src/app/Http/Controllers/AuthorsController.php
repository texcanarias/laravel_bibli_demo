<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Author;

class AuthorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            if(true){
                $authors = Author::all();

                return response()->json($authors, 200);
            } else {
                return response()->json(["message" => "Unauthenticated."], 401);
            }
        } catch (QueryException $ex) {
            return response()->json(['error' => 'Data base error.'], 500);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Error en la consulta'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if(true){
                $author = new Author();
                $author->name = $request->input('name');
                $author->save();

                return response()->json($author, 200);
            } else {
                return response()->json(["message" => "Unauthenticated."], 401);
            }
        } catch (QueryException $ex) {
            return response()->json(['error' => 'Data base error.'], 500);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Error en la consulta'], 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if(true){
                $author = Author::find($id);
                return response()->json($author, 200);
            } else {
                return response()->json(["message" => "Unauthenticated."], 401);
            }
        } catch (QueryException $ex) {
            return response()->json(['error' => 'Data base error.'], 500);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Error en la consulta'], 400);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            if(true){
                $author = Author::find($id);
                $author->name = $request->input('name');
                $author->save();

                return response()->json($author, 200);
            } else {
                return response()->json(["message" => "Unauthenticated."], 401);
            }
        } catch (QueryException $ex) {
            return response()->json(['error' => 'Data base error.'], 500);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Error en la consulta'], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if(true){
                Author::where('id', $id)->delete();
                return response()->json([], 200);
            } else {
                return response()->json(["message" => "Unauthenticated."], 401);
            }
        } catch (QueryException $ex) {
            return response()->json(['error' => 'Data base error.'], 500);
        } catch (\Exception $ex) {
            return response()->json(['error' => 'Error en la consulta'], 400);
        }
    }
}
