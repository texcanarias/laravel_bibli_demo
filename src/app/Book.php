<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';

    /**
     * Atributos accesibles
     *
     * @var array
     */
    protected $fillable = [
        'name', 'position', 'from', 'readed'
    ];
}
