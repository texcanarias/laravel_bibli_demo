<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $table = 'authors';

    /**
     * Atributos accesibles
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];    
}
