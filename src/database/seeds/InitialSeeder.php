<?php

use Illuminate\Database\Seeder;

class InitialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('authors')->insert(['name' => 'Camilla Läckberg']);
        \DB::table('authors')->insert(['name' => 'Almudena Grandes']);
        \DB::table('authors')->insert(['name' => 'Santiago Posteguillo']);
        \DB::table('authors')->insert(['name' => 'Elvira Lindo']);
        \DB::table('authors')->insert(['name' => 'Sonsoles Ónega']);
        \DB::table('authors')->insert(['name' => 'Máximo Huerta']);
        \DB::table('authors')->insert(['name' => 'Peridis']);

        \DB::table('books')->insert(['author_id' => 1 , 'name' => 'Mujeres que no perdonan', 'position' => null, 'from' => null]);
        \DB::table('books')->insert(['author_id' => 2 , 'name' => 'La madre de Frankenstein', 'position' => null, 'from' => null]);
        \DB::table('books')->insert(['author_id' => 3 , 'name' => 'Y Julia retó a los dioses', 'position' => null, 'from' => null]);
        \DB::table('books')->insert(['author_id' => 4 , 'name' => 'A corazón abierto', 'position' => null, 'from' => null]);
        \DB::table('books')->insert(['author_id' => 5 , 'name' => 'Mil besos prohibidos', 'position' => null, 'from' => null]);
        \DB::table('books')->insert(['author_id' => 6 , 'name' => 'Con el amor bastaba', 'position' => null, 'from' => null]);
        \DB::table('books')->insert(['author_id' => 7 , 'name' => 'El corazón con que vivo', 'position' => null, 'from' => null]);         
    }
}



