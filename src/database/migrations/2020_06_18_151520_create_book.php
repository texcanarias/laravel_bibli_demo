<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBook extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authors', function (Blueprint $table) {
            $table->id();
            $table->string('name',80)
                ->nullable(false)
                ->comment('Nombre del author');             
            $table->timestamps();                
        });

        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('name',80)
                ->nullable(false)
                ->comment('Nombre del libro');             
            $table->string('position',3)
                ->nullable(true)
                ->comment('Posicion en la biblioteca');             
            $table->string('from',3)
                ->nullable(true)
                ->comment('Origen del libro');   
            $table->boolean('readed')->default(false);                      
            $table->unsignedBigInteger('author_id')
                ->unsigned()
                ->nullable(false)
                ->comment('Autor del libro');             
            $table->foreign('author_id')
                ->references('id')
                ->on('authors');                    
            $table->timestamps();                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');        
        Schema::dropIfExists('authors');
    }
}
